minidb (2.0.8-1) unstable; urgency=medium

  * New upstream release
  * Remove patch now included upstream
  * Update copyright year for upstream

 -- Maxime Werlen <maxime@werlen.fr>  Thu, 25 Apr 2024 21:32:11 +0200

minidb (2.0.7-2) unstable; urgency=medium

  * Add a patch to switch from distutils to setuptools (closes: #1065902)
  * Update copyright years
  * Bump standard version

 -- Maxime Werlen <maxime@werlen.fr>  Mon, 11 Mar 2024 22:39:04 +0100

minidb (2.0.7-1) unstable; urgency=medium

  * New upstream release
  * Updated Standards-Version to 4.6.1

 -- Maxime Werlen <maxime@werlen.fr>  Sun, 18 Sep 2022 21:33:51 +0200

minidb (2.0.6-1) unstable; urgency=medium

  * New upstream release
  * Update copyright years

 -- Maxime Werlen <maxime@werlen.fr>  Tue, 22 Feb 2022 22:08:35 +0100

minidb (2.0.5-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove obsolete field Name from debian/upstream/metadata

  [ Maxime Werlen ]
  * Remove .github folder from this repo
  * Remove duplicated lines in copyright (Thanks to Tobias Frost)
  * Deduplicate build dependency on dh and dh-compat (Thanks to Tobias Frost)
  * Updated Standards-Version to 4.6.0
  * Fix build dependencies (python3 was missing for dh-python)
  * Relace previous superficial test by upstream tests
  * Upload to unstable after Bullseye release

 -- Maxime Werlen <maxime@werlen.fr>  Wed, 06 Oct 2021 21:13:45 +0200

minidb (2.0.5-1) experimental; urgency=medium

  * New upstream release
  * Clean README of status buttons
  * Upstream renamed README to README.md
  * Bump standards version
  * Update copyright years
  * Add a Vcs-Browser field
  * Update uscan config file version
  * Bump to compat 13 and use debhelper-compat virtual package
  * Add an explicit Rules-Requires-Root field (value no)

 -- Maxime Werlen <maxime@werlen.fr>  Sun, 13 Jun 2021 22:38:17 +0200

minidb (2.0.4-1) unstable; urgency=medium

  * New upstream release 2.0.4
  * Change upstream test framework in build dependencies (nose to pytest)
  * Update VCS URL after salsa username policy change (-guest removal)

 -- Maxime Werlen <maxime@werlen.fr>  Sun, 03 May 2020 21:24:01 +0200

minidb (2.0.3-1) unstable; urgency=medium

  * New upstream version 2.0.3 (Closes: #955221)
  * Update Standards version to 4.5.0
  * Update debhelper compat to version 12
  * Configured to use gbp
  * Fixed Build-Depends error

 -- Maxime Werlen <maxime@werlen.fr>  Sun, 29 Mar 2020 14:15:23 +0200

minidb (2.0.2-3) unstable; urgency=medium

  * Updated Standards-Version to 4.1.4
  * Removed X-Python3-Version per Lintian rule ancient-python-version-field
  * Updated extended description to pass lintian checks

 -- Maxime Werlen <maxime@werlen.fr>  Sun, 02 Sep 2018 20:58:25 +0200

minidb (2.0.2-2) unstable; urgency=low

  * Updated extended description from thp.io site
  * Adding missing blank line in ISC license
  * Wrapped d/watch file
  * Update debhelper version in control file to match d/compat

 -- Maxime Werlen <maxime@werlen.fr>  Sun, 25 Feb 2018 14:23:34 +0100

minidb (2.0.2-1) unstable; urgency=low

  * Initial release. (Closes: #887426)

 -- Maxime Werlen <maxime@werlen.fr>  Fri, 09 Feb 2018 06:34:26 +0100
